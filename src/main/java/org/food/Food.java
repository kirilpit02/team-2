package org.food;

public final class Food {
    private String name;
    private String dosage;
    private float caloriesPer100;
    private float fatsPer100;

    public Food(String name,
                String dosage,
                float caloriesPer100,
                float fatsPer100) {
        setName(name);
        setDosage(dosage);
        setCaloriesPer100(caloriesPer100);
        setFatsPer100(fatsPer100);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public float getCaloriesPer100() {
        return caloriesPer100;
    }

    public void setCaloriesPer100(float caloriesPer100) {

        if (caloriesPer100 < 0) {
            throw new IllegalArgumentException("Calories cannot be negative");
        }

        this.caloriesPer100 = caloriesPer100;
    }

    public float getFatsPer100() {

        return fatsPer100;
    }

    public void setFatsPer100(float fatsPer100) {

        if (fatsPer100 < 0) {
            throw new IllegalArgumentException("Fats cannot be negative");
        }

        this.fatsPer100 = fatsPer100;
    }
}
