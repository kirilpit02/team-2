package org.example;

public enum HeightUnit {
    INCHES(2.54f),
    CENTIMETERS(1f);

    private final float factor;

    public float getFactor() {
        return this.factor;
    }

    HeightUnit(float f) {
        this.factor = f;
    }
}
