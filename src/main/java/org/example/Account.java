package org.example;

public final class Account {
    private String firstName;
    private String lastName;
    private int age;
    private float weight;
    private float heightInCentimetres;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeightInCentimetres() {
        return this.heightInCentimetres;
    }

    public void setHeightInCentimetres(float heightInCentimetres) {
        this.heightInCentimetres = heightInCentimetres;
    }

    public Account(String firstName,
                   String lastName,
                   int age,
                   float weight,
                   float height,
                   HeightUnit heightUnit) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAge(age);
        this.setWeight(weight);
        this.setHeightInCentimetres(height * heightUnit.getFactor());
    }
}
