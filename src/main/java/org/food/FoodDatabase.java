package org.food;

import java.util.Dictionary;
import java.util.Hashtable;

public final class FoodDatabase {

    private Dictionary<String, Food> foodList;

    public FoodDatabase() {
        foodList = new Hashtable<>();
    }

    public void addFood(Food food) {
        foodList.put(food.getName(), food);
    }

    public Food getFood(String name) {
        return foodList.get(name);
    }

    public Dictionary<String, Food> getAllFoods() {
        return foodList;
    }

}
