package org.example;

import java.util.Scanner;

public final class AccountManager {
    public Account createAccount() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please provide your name in the"
                + " following format: <first_name> <last_name>");
        String fullName = scanner.nextLine();
        String[] names = fullName.split(" ");

        System.out.println("Please provide your age: ");
        int age = Integer.parseInt(scanner.nextLine());

        System.out.println("Please provide your weight in"
                + " kilograms: [Example: 50.55]");
        float weight = Float.parseFloat(scanner.nextLine());

        System.out.println("Please provide your height:"
                + " [Example for feet: 5'8\"; Example for cm: 175cm]");
        String heightInfo = scanner.nextLine();
        float height;
        HeightUnit unit;
        if (heightInfo.contains("cm")) {
            unit = HeightUnit.CENTIMETERS;
            height = Float.parseFloat(heightInfo.split("c")[0].trim());
        } else {
            unit = HeightUnit.INCHES;
            String[] feetInches = heightInfo.split("['\"]");
            height = Integer.parseInt(feetInches[0].trim()) * 12f
                    + Integer.parseInt(feetInches[1].trim());
        }

        return new Account(names[0], names[1], age, weight, height, unit);
    }

    public static void main(String[] args) {
        AccountManager accountManager = new AccountManager();
        Account acc = accountManager.createAccount();
        System.out.println(acc.getAge());
    }
}
